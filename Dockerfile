FROM openjdk:8
WORKDIR /app
COPY ./target/Api-Investimentos-*.jar .
CMD java -jar Api-Investimentos-*.jar
